import os
import numpy as np
import pandas as pd
from keras.models import Sequential, Model
from keras.layers import Input, Dense, Dropout, Flatten
from keras import optimizers, regularizers
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
from keras import applications
from keras.applications.vgg16 import preprocess_input
import keras.backend as K
from PIL import Image, ImageOps

def get_trainset(categories_idx, trainpath):
    trainset_img = []
    trainset_cat = []
    for cat_idx, cat_name  in categories_idx.items():
        print(cat_idx, cat_name)
        catdir = os.path.join(trainpath, cat_name)
        for img_filename in os.listdir(catdir):
            img = ImageOps.fit(load_img(os.path.join(catdir, img_filename)), (224, 224), method=Image.LANCZOS)
            arr_img = img_to_array(img)
            prepro_img = preprocess_input(arr_img) #/ 255.#- vgg_mean
            trainset_img.append(prepro_img)
            trainset_cat.append(cat_idx)

    xs, ys = np.asarray(trainset_img), np.asarray(trainset_cat) #np.load('trainset_x_224.npy'), np.load('trainset_y.npy')
    one_hot_y = np.zeros((ys.shape[0], 9))
    one_hot_y[np.arange(ys.shape[0]), ys] = 1
    return xs, one_hot_y

def get_testset(categories_idx, testpath):
    testset_img = []
    testset_fname = []
    for img_filename in os.listdir(testpath):
        img = preprocess_input(img_to_array(ImageOps.fit(load_img(os.path.join(testpath, img_filename)), (224, 224), method=Image.LANCZOS))) #/ 255.#- vgg_mean
        testset_img.append(img)#[..., ::-1])
        testset_fname.append(img_filename)
    return testset_fname, np.asarray(testset_img)

def build_model():
    vggmodel = applications.VGG16(include_top=False, weights='imagenet', input_shape=(224, 224, 3))

    flat = Flatten()(vggmodel.output)
    fc1 = Dense(128, activation='relu', kernel_regularizer=regularizers.l2(0.01))(flat)
    drop1 = Dropout(0.5)(fc1)
    fc_softmax = Dense(9, activation='softmax')(drop1)
    finemodel = Model(inputs=vggmodel.input, outputs=fc_softmax)

    for layer in finemodel.layers[:-7]:
        layer.trainable = False

    finemodel.compile(loss='categorical_crossentropy', optimizer=optimizers.SGD(1e-4, momentum=0.9), metrics=['accuracy'])
    return finemodel

def train(model, xs, ys, epochs=8, batch_size=16):
    p = np.random.permutation(ys.shape[0])
    model.fit(xs[p], ys[p], epochs=epochs, batch_size=batch_size)

def test(model, test_xs, testset_fname, csv_filename, categories_idx, batch_size=16):
    preds = model.predict(test_xs, batch_size=batch_size)
    res = list(zip(list(testset_fname), list(map(lambda p: categories_idx[p], np.argmax(preds, axis=1)))))
    pd.DataFrame(res, columns=['filename', 'label']).to_csv(csv_filename, index=False)

categories_idx = {i: k for (i, k) in enumerate([
    'beauty-personal_care-hygiene',
    'clothing',
    'communications',
    'footwear',
    'household-office_furniture-furnishings',
    'kitchen_merchandise',
    'personal_accessories',
    'sports_equipment',
    'toys-games',
])}

train_xs, train_ys = get_trainset(categories_idx, 'data/train')
model = build_model()
train(model, train_xs, train_ys)

test_fname, test_xs = get_testset(categories_idx, 'data/test')
test(model, test_xs, test_fname, 'solution_new.csv', categories_idx)
